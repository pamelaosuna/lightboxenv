import gym
from gym import spaces
import numpy as np
import random as rd

class Lightbox(gym.Env):
    """Custom Environment that follows gym interface"""
    metadata = {'render.modes': ['human']}

    def __init__(self, reward_function = None):
        super(Lightbox, self).__init__()    
        # Define action and observation space
        # They must be gym.spaces objects    
        N_DISCRETE_ACTIONS = 20
        self.action_space = spaces.Discrete(N_DISCRETE_ACTIONS)    
        self.observation_space = spaces.Box(low=0, high=1, shape=(20,), dtype=np.uint8)
        # Dictionary of light dependencies
        self.dependencies = {10: [1, 4, 7], 11: [2, 5], 12: [3,6], 13: [2, 3], 14: [5, 6], 15: [7, 8, 9], 16: [10, 11], 17: [11, 12], 18: [13, 14], 19: [14, 15], 20: [17, 18]}

    def step(self, action):
        # Execute one time step within the environment
        done = False
        
        if action not in self.dependencies.keys():
            self.state[action-1] = 1
        else:
            # Check dependencies
            for light in self.dependencies[action]:
                if not self.state[light-1]:
                    obs = self.state
                    reward = 0
                    done = False
                    info = {}
                    # Reset if dependencies are not satisfied
                    self.state = np.array([0]*20)

                    return obs, reward, done, info
            # Turn light on (with probability of 0.9) if dependencies are satifistied
            if rd.random() < 0.9:
                self.state[action-1] = 1
        #print(self.state)


        obs = self.state
        reward = 0
        
        if np.all(obs):
            done = True
        

        return obs, reward, done, {}

    def reset(self):
        # Reset the state of the environment to an initial state
        self.state = np.array([0]*20)
        #print(self.state)

    def render(self, mode='human', close=False):
    # Render the environment to the screen
        pass
