import gym
from gym import spaces
import numpy as np
import random as rd
import copy

class CoffeeTask(gym.Env):
    """
    
    Environement that follows the coffee task described in [Boutilier et         al., 1994].

    > States: a tuple (L, U, R, W, HCU, HCR) of 6 binary variables.

    L: 1 if robot is at the coffee shop, 0 if it is at the office
    U: 1 if robot does has an umbrella, 0 otherwise
    R: 1 if it is raining, 0 otherwise
    W: 1 if robot is wet, 0 otherwise
    HCR: 1 if robot has coffee, 0 otherwise
    HCU: 1 if user has coffee, 0 otherwise

    There are 64 states in total.

    > Actions:

    0: go to the opposite location
    1: get umbrella
    2: buy coffee
    3: deliver coffee

    """
    metadata = {'render.modes': ['human']}

    def __init__(self):
        super(CoffeeTask, self).__init__()    
        # Define action and observation space
        # They must be gym.spaces objects
        N_DISCRETE_ACTIONS = 4
        self.action_space = spaces.Discrete(N_DISCRETE_ACTIONS)    
        # State domain definition
        self.observation_space = spaces.Box(low = 0, high = 1, shape = (6, ), dtype=np.uint8)
        self.is_deterministic = False
    
    def step(self, action, probs = None):
        # Execute one time step within the environment
        done = False

        if probs is None and not self.is_deterministic:
            probs = [0.9]*5 + [0.8] + [0.9]*2
        elif self.is_deterministic:
            probs = [1.0]*8

        # As described in the STRIPS table of the article : https://www.aaai.org/Papers/AAAI/1994/AAAI94-156.pdf
        if action == 0: # go
            if self.state[0] == 0: # robot is at the office
                if rd.random() < probs[0]:
                    self.state[0] = 1 # robot gets to the coffee shop
            elif self.state[0] == 1: # robot is at the coffee shop
                if rd.random() < probs[1]:
                    self.state[0] = 0 # robot gets to the office
            if self.state[2] == 1 and self.state[1] == 0: # it is raining and robot does not have an umbrella
                if rd.random() < probs[2]:
                    self.state[3] = 1 # robot gets wet
        elif action == 1: # get umbrella
            if self.state[1] == 0 and self.state[0] == 0: # robot is at the office and does not have an umbrella
                if rd.random() < probs[3]:
                    self.state[1] = 1 # robot gets an umbrella
        elif action == 2: # buy coffee
            if self.state[0] == 1 and self.state[4] == 0: # robot is at the coffee shop and does not have coffee
                if rd.random() < probs[4]:
                    self.state[4] = 1 # robot gets coffee
        elif action == 3:
            if self.state[0] == 0 and self.state[4] == 1: # robot is at the office and has coffee
                if rd.random() < probs[5]:
                    self.state[5] = 1 # user gets coffee
                    self.state[4] = 0 # robot delivered coffee
                elif rd.random() < probs[6]:
                    self.state[4] = 0 # robot threw the coffee =(
            elif self.state[0] == 1 and self.state[4] == 1: # robot is at the coffee shop and has coffee
                if rd.random() < probs[7]:
                    self.state[4] = 0 # robot wrongly deliviered coffee
        else:
            print("Action '" + str(action) + "' is not defined")

        
        obs = copy.deepcopy(self.state)
        
        if self.state[5] == 1: # user has coffee
            done = True
            # print(">> User got coffee. Environment solved!")
            if self.state[3] == 1: # robot is wet
                reward = 0.9
            else: # robot is dry
                reward = 1.0
        elif self.state[3] == 0:
            reward = 0.1
        else:
            reward = 0.0


        return obs, reward, done, {}

    def reset(self):
        # Reset the state of the environment to an initial state
        if not self.is_deterministic:
            self.state = np.array([0, 0, int(rd.random()<0.8), 0, 0, 0])
        else:
            self.state = np.array([0,0,1,0,0,0])
        #print(self.state)
        return copy.deepcopy(self.state)

    def render(self, mode='human', close=False):
    # Render the environment to the screen
        pass

    def set_deterministic(self):
        self.is_deterministic = True

    def getDBNs(self):
        """
        Return DBN for each action

        Reminder:

        > Actions:
            0: go
            1: get umbrella
            2: buy coffee
            3: deliver coffee
        
        > Variables defining a state:
            (0) L: 1 if robot is at the coffee shop, 0 if it is at the office
            (1) U: 1 if robot does has an umbrella, 0 otherwise
            (2) R: 1 if it is raining, 0 otherwise
            (3) W: 1 if robot is wet, 0 otherwise
            (4) HCR: 1 if robot has coffee, 0 otherwise
            (5) HCU: 1 if user has coffee, 0 otherwise


        """
        dbns = {0: {3: [1, 2]}, 
                1: {1: [0]}, 
                2: {4: [0]}, 
                3: {4: [0], 5: [0, 4]}
                }
        return dbns
        pass